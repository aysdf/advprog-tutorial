package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.role = "CTO";
        if (salary < 100000.00) {
            throw new IllegalArgumentException("CTO Salary lower than 100000.00");
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}

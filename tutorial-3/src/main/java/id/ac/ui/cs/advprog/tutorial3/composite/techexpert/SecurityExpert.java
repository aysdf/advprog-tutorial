package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.role = "Security Expert";
        if (salary < 70000.00) {
            throw new IllegalArgumentException("Security Expert Salary lower than 70000.00");
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}

package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class CVController {

    @GetMapping("/cv")
    public String cv(@RequestParam(value = "visitor", required = false) String visitor, Model model) {
        String cv = "This is my CV";
        if (visitor != null) {
            model.addAttribute("visitor", visitor);
            cv = visitor + ", I hope you interested to hire me";
            model.addAttribute("cv", cv);
        } else {
            model.addAttribute("cv", cv);
        }
        return "cv";
    }
}
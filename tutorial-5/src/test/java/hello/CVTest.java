/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {CVController.class})
public class CVTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void cvPage() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Ghinaa Soraya Andria")));
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Jakarta, July 8, 1997")));
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Jl. Matraman Dalam II No. 10 RT 08 RW 008 Pegangsaan Jakarta Pusat 10320")));
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("A Computer Science senior with a passion and experience in design and development.")));
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("A hard-worker and eager learner. Driven to create to make impact in people's lives.")));
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Also interested with woman empowerment, social justice and art.")));
    }

    @Test
    public void cvWithVisitor() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", "Greg"))
                .andExpect(content().string(containsString("Greg, I hope you interested to hire me")));
    }

    @Test
    public void cvWithoutVisitor() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")));
    }

}

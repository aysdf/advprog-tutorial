package exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.SalsaSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SauceTest {

    private Sauce sauce;
    private Class<?> marinaraSauceClass;
    private Class<?> plumTomatoSauceClass;
    private Class<?> salsaSauceClass;
    private Class<?> sauceClass;

    @Before
    public void setUp() throws Exception {
        marinaraSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce");
        plumTomatoSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce");
        salsaSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.SalsaSauce");
        sauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce");
    }

    @Test
    public void testMarinaraSauceIsASauce() {
        Collection<Type> classInterfaces = Arrays.asList(marinaraSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testMarinaraSauceToString() {
        sauce = new MarinaraSauce();
        assertEquals(sauce.toString(), "Marinara Sauce");
    }

    @Test
    public void testPlumTomatoSauceIsASauce() {
        Collection<Type> classInterfaces = Arrays.asList(plumTomatoSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testPlumTomatoSauceToString() {
        sauce = new PlumTomatoSauce();
        assertEquals(sauce.toString(), "Tomato sauce with plum tomatoes");
    }

    @Test
    public void testSalsaSauceIsASauce() {
        Collection<Type> classInterfaces = Arrays.asList(salsaSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testSalsaSauceToString() {
        sauce = new SalsaSauce();
        assertEquals(sauce.toString(), "Salsa Sauce");
    }

    @Test
    public void testSauceIsAPublicInterface() {
        int classModifiers = sauceClass.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testSauceHasToStringAbstractMethod() throws Exception {
        Method toString = sauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

}
package exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

class PizzaStoreTest {

    private PizzaStore dpPizzaStore;
    private PizzaStore nyPizzaStore;

    @BeforeEach
    void setUp() {
        dpPizzaStore = spy(new DepokPizzaStore());
        nyPizzaStore = spy(new NewYorkPizzaStore());
    }

    @Test
    void testDepokPizzaStore() {
        dpPizzaStore.orderPizza("cheese");
        verify(dpPizzaStore).orderPizza("cheese");
        dpPizzaStore.orderPizza("clam");
        verify(dpPizzaStore).orderPizza("clam");
        dpPizzaStore.orderPizza("veggie");
        verify(dpPizzaStore).orderPizza("veggie");
    }

    @Test
    void testNewYorkPizzaStore() {
        nyPizzaStore.orderPizza("cheese");
        verify(nyPizzaStore).orderPizza("cheese");
        nyPizzaStore.orderPizza("clam");
        verify(nyPizzaStore).orderPizza("clam");
        nyPizzaStore.orderPizza("veggie");
        verify(nyPizzaStore).orderPizza("veggie");
    }

}
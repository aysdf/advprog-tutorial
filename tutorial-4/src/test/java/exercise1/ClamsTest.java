package exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.GreenClams;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClamsTest {

    private Clams clams;
    private Class<?> clamsClass;
    private Class<?> freshClamsClass;
    private Class<?> frozenClamsClass;
    private Class<?> greenClamsClass;

    @Before
    public void setUp() throws Exception {
        clamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams");
        freshClamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams");
        frozenClamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams");
        greenClamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.GreenClams");
    }

    @Test
    public void testFreshClamsIsAClams() {
        Collection<Type> classInterfaces = Arrays.asList(freshClamsClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testFreshClamsToString() {
        clams = new FreshClams();
        assertEquals(clams.toString(), "Fresh Clams from Long Island Sound");
    }

    @Test
    public void testClamsIsAPublicInterface() {
        int classModifiers = clamsClass.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testClamsHasToStringAbstractMethod() throws Exception {
        Method toString = clamsClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testFrozenClamsIsAClams() {
        Collection<Type> classInterfaces = Arrays.asList(frozenClamsClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testFrozenClamsToString() {
        clams = new FrozenClams();
        assertEquals(clams.toString(), "Frozen Clams from Chesapeake Bay");
    }

    @Test
    public void testGreenClamsIsAClams() {
        Collection<Type> classInterfaces = Arrays.asList(greenClamsClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testGreenClamsToString() {
        clams = new GreenClams();
        assertEquals(clams.toString(), "Green Clams from Tampa Bay");
    }

}
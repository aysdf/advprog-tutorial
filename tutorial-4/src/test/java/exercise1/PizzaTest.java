package exercise1;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PizzaTest {

    private Pizza pizza;

    @BeforeEach
    void setUp() {
        pizza = spy(new Pizza() {

            @Override
            public void prepare() {
                System.out.println("Preparing " + name);
            }
        });

        pizza.setName("Empty Pizza");
    }

    @Test
    void testEmptyPizzaActions() {
        pizza.prepare();
        verify(pizza).prepare();
        pizza.bake();
        verify(pizza).bake();
        pizza.cut();
        verify(pizza).cut();
        pizza.box();
        verify(pizza).box();
    }

    @Test
    void testSetPizzaName() {
        pizza.setName("Null Pizza");
        assertEquals(pizza.getName(), "Null Pizza");
    }

    @Test
    void testGetPizzaName() {
        assertEquals(pizza.getName(), "Empty Pizza");
    }

}
package exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CheeseCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DoughTest {

    private Dough dough;
    private Class<?> cheeseCrustDoughClass;
    private Class<?> doughClass;
    private Class<?> thickCrustDoughClass;
    private Class<?> thinCrustDoughClass;

    @Before
    public void setUp() throws Exception {
        cheeseCrustDoughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CheeseCrustDough");
        doughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough");
        thickCrustDoughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough");
        thinCrustDoughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough");
    }

    @Test
    public void testCheeseCrustDoughIsADough() {
        Collection<Type> classInterfaces = Arrays.asList(cheeseCrustDoughClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testCheeseCrustDoughToString() {
        dough = new CheeseCrustDough();
        assertEquals(dough.toString(), "Extra Cheese Crust Dough");
    }

    @Test
    public void testDoughIsAPublicInterface() {
        int classModifiers = doughClass.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testDoughHasToStringAbstractMethod() throws Exception {
        Method toString = doughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testThickCrustDoughIsADough() {
        Collection<Type> classInterfaces = Arrays.asList(thickCrustDoughClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testThickCrustDoughToString() {
        dough = new ThickCrustDough();
        assertEquals(dough.toString(), "ThickCrust style extra thick crust dough");
    }

    @Test
    public void testThinCrustDoughIsADough() {
        Collection<Type> classInterfaces = Arrays.asList(thinCrustDoughClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testThinCrustDoughToString() {
        dough = new ThinCrustDough();
        assertEquals(dough.toString(), "Thin Crust Dough");
    }

}
package exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VeggieTest {

    private Veggies veggie;
    private Class<?> bellPepperClass;
    private Class<?> blackOlivesClass;
    private Class<?> eggplantClass;
    private Class<?> garlicClass;
    private Class<?> mushroomClass;
    private Class<?> onionClass;
    private Class<?> redPepperClass;
    private Class<?> spinachClass;
    private Class<?> veggiesClass;


    @Before
    public void setUp() throws Exception {
        bellPepperClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BellPepper");
        blackOlivesClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives");
        eggplantClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant");
        garlicClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic");
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom");
        onionClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion");
        redPepperClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper");
        spinachClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach");
        veggiesClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies");
    }

    @Test
    public void testBellPepperIsAVeggies() {
        Collection<Type> classInterfaces = Arrays.asList(bellPepperClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testBellPepperToString() {
        veggie = new BellPepper();
        assertEquals(veggie.toString(), "Bell Pepper");
    }

    @Test
    public void testBlackOlivesIsAVeggies() {
        Collection<Type> classInterfaces = Arrays.asList(blackOlivesClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testBlackOlivesToString() {
        veggie = new BlackOlives();
        assertEquals(veggie.toString(), "Black Olives");
    }

    @Test
    public void testEggplantIsAVeggies() {
        Collection<Type> classInterfaces = Arrays.asList(eggplantClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testEggplantToString() {
        veggie = new Eggplant();
        assertEquals(veggie.toString(), "Eggplant");
    }

    @Test
    public void testGarlicIsAVeggies() {
        Collection<Type> classInterfaces = Arrays.asList(garlicClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testGarlicToString() {
        veggie = new Garlic();
        assertEquals(veggie.toString(), "Garlic");
    }

    @Test
    public void testMushroomIsAVeggies() {
        Collection<Type> classInterfaces = Arrays.asList(mushroomClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testMushroomToString() {
        veggie = new Mushroom();
        assertEquals(veggie.toString(), "Mushrooms");
    }

    @Test
    public void testOnionIsAVeggies() {
        Collection<Type> classInterfaces = Arrays.asList(onionClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testOnionToString() {
        veggie = new Onion();
        assertEquals(veggie.toString(), "Onion");
    }

    @Test
    public void testRedPepperIsAVeggies() {
        Collection<Type> classInterfaces = Arrays.asList(redPepperClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testRedPepperToString() {
        veggie = new RedPepper();
        assertEquals(veggie.toString(), "Red Pepper");
    }

    @Test
    public void testSpinachIsAVeggies() {
        Collection<Type> classInterfaces = Arrays.asList(spinachClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testSpinachToString() {
        veggie = new Spinach();
        assertEquals(veggie.toString(), "Spinach");
    }



    @Test
    public void testVeggiesIsAPublicInterface() {
        int classModifiers = veggiesClass.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testVeggiesHasToStringAbstractMethod() throws Exception {
        Method toString = veggiesClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

}
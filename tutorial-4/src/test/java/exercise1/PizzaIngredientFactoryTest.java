package exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CheddarCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.GreenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.SalsaSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PizzaIngredientFactoryTest {

    private Clams freshClam;
    private Clams greenClam;
    private Cheese cheddarCheese;
    private Cheese reggianoCheese;
    private Dough thickCrustDough;
    private Dough thinCrustDough;
    private PizzaIngredientFactory dpIngredientFactory;
    private PizzaIngredientFactory nyIngredientFactory;
    private Sauce marinaraSauce;
    private Sauce salsaSauce;
    private Veggies bellPepper;
    private Veggies blackOlives;
    private Veggies eggplant;
    private Veggies garlic;
    private Veggies mushroom;
    private Veggies onion;
    private Veggies redPepper;
    private Veggies spinach;

    @Before
    public void setUp() {
        freshClam = new FreshClams();
        greenClam = new GreenClams();
        cheddarCheese = new CheddarCheese();
        reggianoCheese = new ReggianoCheese();
        thickCrustDough = new ThickCrustDough();
        thinCrustDough = new ThinCrustDough();
        dpIngredientFactory = new DepokPizzaIngredientFactory();
        nyIngredientFactory = new NewYorkPizzaIngredientFactory();
        marinaraSauce = new MarinaraSauce();
        salsaSauce = new SalsaSauce();
        bellPepper = new BellPepper();
        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        garlic = new Garlic();
        mushroom = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
    }

    @Test
    public void testDepokIngredientFactory() {
        Clams depokClam = dpIngredientFactory.createClam();
        assertEquals(depokClam.toString(), greenClam.toString());

        Cheese depokCheese = dpIngredientFactory.createCheese();
        assertEquals(depokCheese.toString(), cheddarCheese.toString());

        Dough depokDough = dpIngredientFactory.createDough();
        assertEquals(depokDough.toString(), thickCrustDough.toString());

        Sauce depokSauce = dpIngredientFactory.createSauce();
        assertEquals(depokSauce.toString(), salsaSauce.toString());

        String[] veggiesComparison = {bellPepper.toString(), blackOlives.toString(), eggplant.toString(), spinach.toString()};
        Veggies[] depokVeggies = dpIngredientFactory.createVeggies();

        for (int i = 0; i < veggiesComparison.length; i++) {
            assertEquals(depokVeggies[i].toString(), veggiesComparison[i]);
        }
    }

    @Test
    public void testNewYorkIngredientFactory() {
        Clams newYorkClam = nyIngredientFactory.createClam();
        assertEquals(newYorkClam.toString(), freshClam.toString());

        Cheese newYorkCheese = nyIngredientFactory.createCheese();
        assertEquals(newYorkCheese.toString(), reggianoCheese.toString());

        Dough newYorkDough = nyIngredientFactory.createDough();
        assertEquals(newYorkDough.toString(), thinCrustDough.toString());

        Sauce newYorkSauce = nyIngredientFactory.createSauce();
        assertEquals(newYorkSauce.toString(), marinaraSauce.toString());

        String[] veggiesComparison = {garlic.toString(), onion.toString(), mushroom.toString(), redPepper.toString()};
        Veggies[] newYorkVeggies = nyIngredientFactory.createVeggies();

        for (int i = 0; i < veggiesComparison.length; i++) {
            assertEquals(newYorkVeggies[i].toString(), veggiesComparison[i]);
        }
    }

}
package exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CheeseTest {

    private Cheese cheese;
    private Class<?> cheddarCheeseClass;
    private Class<?> cheeseClass;
    private Class<?> mozzarellaCheeseClass;
    private Class<?> parmesanCheeseClass;
    private Class<?> reggianoCheeseClass;

    @Before
    public void setUp() throws Exception {
        cheddarCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CheddarCheese");
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese");
        mozzarellaCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese");
        parmesanCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese");
        reggianoCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese");
    }

    @Test
    public void testCheddarCheeseIsACheese() {
        Collection<Type> classInterfaces = Arrays.asList(cheddarCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testCheddarCheeseToString() {
        cheese = new CheddarCheese();
        assertEquals(cheese.toString(), "Shredded Cheddar");
    }

    @Test
    public void testCheeseIsAPublicInterface() {
        int classModifiers = cheeseClass.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testCheeseHasToStringAbstractMethod() throws Exception {
        Method toString = cheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testMozzarellaCheeseIsACheese() {
        Collection<Type> classInterfaces = Arrays.asList(mozzarellaCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testMozzarellaCheeseToString() {
        cheese = new MozzarellaCheese();
        assertEquals(cheese.toString(), "Shredded Mozzarella");
    }

    @Test
    public void testParmesanCheeseIsACheese() {
        Collection<Type> classInterfaces = Arrays.asList(parmesanCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testParmesanCheeseToString() {
        cheese = new ParmesanCheese();
        assertEquals(cheese.toString(), "Shredded Parmesan");
    }

    @Test
    public void testReggianoCheeseIsACheese() {
        Collection<Type> classInterfaces = Arrays.asList(reggianoCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testReggianoCheeseToString() {
        cheese = new ReggianoCheese();
        assertEquals(cheese.toString(), "Reggiano Cheese");
    }

}
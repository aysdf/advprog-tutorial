package exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CheeseCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

class VeggiePizzaTest {

    private Pizza pizza;

    @BeforeEach
    void setUp() {
        pizza = spy(new VeggiePizza(new PizzaIngredientFactory() {
            @Override
            public Dough createDough() {
                return new CheeseCrustDough();
            }

            @Override
            public Sauce createSauce() {
                return new PlumTomatoSauce();
            }

            @Override
            public Cheese createCheese() {
                return new MozzarellaCheese();
            }

            @Override
            public Veggies[] createVeggies() {
                Veggies[] veggies = {new BlackOlives(), new Spinach(), new Eggplant()};
                return veggies;
            }

            @Override
            public Clams createClam() {
                return null;
            }
        }));

        pizza.setName("Example Veggie Pizza");
    }

    @Test
    void testPrepareEmptyCheesePizza() {
        pizza.prepare();
        verify(pizza).prepare();
    }

}